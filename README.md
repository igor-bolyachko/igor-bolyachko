# Професійний курс по Next.js

Привіт! 🚀

Це проєкт [Next.js](https://nextjs.org/), створений з використанням [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Початок роботи

1) Встановіть залежності:

```bash
npm install
# or
yarn install
```

2) Запустіть сервер розробки:

```bash
npm run dev
# or
yarn dev
```

Відкрийте [http://localhost:3000](http://localhost:3000) у вашому браузері, щоб побачити результат.

Ви можете розпочати редагування сторінки, змінивши файл `src/app/page.js`. Сторінка автоматично оновлюватиметься під час редагування файлу.

[API routes](https://nextjs.org/docs/app/building-your-application/routing/route-handlers) можна отримати за адресою [http://localhost:3000/api/hello](http://localhost:3000/api/hello). Цей ендпоінт можна змінити в файлі `src/app/api/hello/route.js`.

##Дізнайтеся більше

Щоб дізнатися більше про Next.js, ознайомтеся з такими ресурсами:

- [Next.js Documentation](https://nextjs.org/docs) - дізнайтесь про функції та API Next.js.
Ви можете перевірити репозиторій [the Next.js GitHub repository](https://github.com/vercel/next.js/) - ваші коментарі та внески вітаються!

##Деплой на Vercel

Найпростіший спосіб задеплоїти ваш додаток Next.js - використовувати [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) від творців Next.js.

Докладнішу інформацію щодо розгортання Next.js можна знайти у [нашій документації з розгортання](https://nextjs.org/docs/deployment).

